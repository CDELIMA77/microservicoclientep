package br.com.mastertech.msclientep;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteServices clienteServices;

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Cliente>> buscarCliente(@PathVariable Long id){
        Optional<Cliente> clienteOptional = clienteServices.buscarPorId(id);
        if (clienteOptional.isPresent()){
            return ResponseEntity.status(200).body(clienteOptional);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Cliente> incluirPorta(@RequestBody Cliente cliente) {
        try { clienteServices.salvarCliente(cliente); }
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(201).body(cliente);
    }
}
