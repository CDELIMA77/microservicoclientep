package br.com.mastertech.msclientep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MsclientepApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsclientepApplication.class, args);
	}

}
