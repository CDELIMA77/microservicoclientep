package br.com.mastertech.msclientep;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServices {

    @Autowired
    private ClienteRepository clienteRepository;

    public Optional<Cliente> buscarPorId(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return clienteOptional;
    }

    public Cliente salvarCliente(Cliente cliente) throws ObjectNotFoundException {
        clienteRepository.save(cliente);
        return cliente;
    }
}
